import "./App.css";
import FixedContainer from "./components/container/container";
import React from "react";
import CartContextProvider from "./context/cartCantext";
import { QueryClientProvider, QueryClient } from "react-query";

const queryClient = new QueryClient();

function App() {
  return (
    <CartContextProvider>
      <QueryClientProvider client={queryClient}>
        <div className="App">
          <FixedContainer className="container" />
        </div>
      </QueryClientProvider>
    </CartContextProvider>
  );
}

export default App;
