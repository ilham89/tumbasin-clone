import React, { useState, createContext, useEffect } from "react";

export const CartContext = createContext();

const CartContextProvider = (props) => {
  const [cart, setCart] = useState([]);
  const [totalPrice, setTotalPrice] = useState(0);
  const [cartUpdated, setCartUpdated] = useState(true);
  useEffect(() => {
    const carts = JSON.parse(localStorage.getItem("cart"));
    const prices = JSON.parse(localStorage.getItem("price"));
    if (carts) {
      setCart(carts);
      setTotalPrice(prices);
    }
  }, []);

  const calculateTotal = (list) => {
    if (list.length > 0) {
      const sum = (item) => item.reduce((x, y) => x + y);
      let total = sum(list.map((product) => Number(product.totalPrice)));
      setTotalPrice(total);
      localStorage.setItem("price", total);
      setCartUpdated(!cartUpdated);
    } else {
      setTotalPrice(0);
      localStorage.setItem("price", 0);
      setCartUpdated(!cartUpdated);
    }
  };

  const addCart = (item) => {
    const cartData = [...cart, { ...item, total: 1, totalPrice: item.price }];
    setCart(cartData);
    calculateTotal(cartData);
    localStorage.setItem("cart", JSON.stringify(cartData));
  };

  const increaseCart = (item) => {
    const objIndex = cart.findIndex((select) => {
      return select.id === item.id;
    });
    let cartData = cart;
    cartData[objIndex].total += 1;
    cartData[objIndex].totalPrice =
      cartData[objIndex].total * cartData[objIndex].price;
    setCart(cartData);
    calculateTotal(cartData);
    localStorage.setItem("cart", JSON.stringify(cartData));
  };

  const decreaseCart = (item) => {
    const objIndex = cart.findIndex((select) => {
      return select.id === item.id;
    });
    let cartData = cart;
    if (cartData[objIndex].total > 1) {
      cartData[objIndex].total -= 1;
      cartData[objIndex].totalPrice =
        cartData[objIndex].total * cartData[objIndex].price;
      setCart(cartData);
      calculateTotal(cartData);
      localStorage.setItem("cart", JSON.stringify(cartData));
    } else {
      const newCart = cart.filter((obj) => {
        return obj.id !== item.id;
      });
      setCart(newCart);
      calculateTotal(newCart);
      localStorage.setItem("cart", JSON.stringify(newCart));
    }
  };

  //   const value = ;
  return (
    <CartContext.Provider
      value={{
        cart,
        addCart,
        totalPrice,
        calculateTotal,
        increaseCart,
        decreaseCart,
      }}
    >
      {props.children}
    </CartContext.Provider>
  );
};

export default CartContextProvider;
