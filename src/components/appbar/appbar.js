import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import SearchAppBar from "../search/search";
import PaperPasar from "../content/paperpasar";
import Fade from "@material-ui/core/Fade";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    // width: "100%",
    position: "absolute",
    zIndex: 999,
    width: "100%",
    maxWidth: 444,
    // "& > *": {
    //   width: 475,
    //   height: 120,
    // },
  },
}));

export default function AppBar({ onTop }) {
  const classes = useStyles();

  return (
    <Fade in={onTop}>
      <div className={classes.root}>
        <Paper
          variant="outlined"
          square
          style={{
            background: "linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)",
            width: "100%",
            height: 110,
          }}
        >
          <SearchAppBar />
        </Paper>
        <PaperPasar />
      </div>
    </Fade>
  );
}
