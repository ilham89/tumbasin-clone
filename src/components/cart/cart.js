import React, { useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import { CartContext } from "../../context/cartCantext";
import CurrentFormatter from "../currency-formatter";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    bottom: 60,
    width: "100%",
    position: "fixed",
    maxWidth: 444,
    zIndex: 999,
    justifyContent: "center",
    "& > *": {
      height: 45,
      borderRadius: 8,
      backgroundColor: "rgb(241, 91, 93)",
      color: "white",
      width: "95%",
    },
  },
  detail: {
    display: "flex",
    flexDirection: "column",
  },
  icon: {
    width: 17,
    height: 17,
  },
  cart: {
    padding: 8,
    margin: "0px 10px 0px 10px",
  },
}));

export default function Cart() {
  const classes = useStyles();
  // const dataContext = useContext(CartContext);
  const { cart, totalPrice } = useContext(CartContext);
  // const totalPrice = (arr) =>
  //   arr.reduce((sum, { curr, product }) => sum + curr * product, 0);

  return (
    <div>
      {cart.length > 0 && (
        <div className={classes.root}>
          <Paper variant="outlined" style={{ marginTop: 50 }}>
            <Grid
              container
              direction="row"
              justify="space-between"
              alignItems="center"
              className={classes.cart}
            >
              <Grid item xs={10} className={classes.detail}>
                <span style={{ fontSize: 12 }}>
                  {cart.length} item | {CurrentFormatter.format(totalPrice)}
                </span>
                <span style={{ fontSize: 10 }}>Pasar Bulu Semarang</span>
              </Grid>
              <Grid item xs={2}>
                <ShoppingCartIcon
                  className={classes.icon}
                  style={{ width: 17, height: 17 }}
                />
              </Grid>
            </Grid>
          </Paper>
        </div>
      )}
    </div>
  );
}
