import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import BottomNavigation from "@material-ui/core/BottomNavigation";
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction";
import StorefrontIcon from "@material-ui/icons/Storefront";
import ReceiptIcon from "@material-ui/icons/Receipt";
import LiveHelpIcon from "@material-ui/icons/LiveHelp";
import PermIdentityIcon from "@material-ui/icons/PermIdentity";
import "../bottom-navigation/style.css";

const useStyles = makeStyles({
  root: {
    // width: 475,
    height: 50,
    position: "fixed",
    bottom: 0,
    borderRadius: 3,
    border: "0.1px solid #dbdbdb",
    maxWidth: 444,
    width: "100%",
  },
  actionItem: {
    "&$selected": {
      color: "#F15B5D",
    },
  },
  selected: {},
  icon: {
    width: 30,
    height: 30,
    padding: "2px",
  },
});

export default function ButtonNav() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  return (
    <BottomNavigation
      value={value}
      onChange={(event, newValue) => {
        setValue(newValue);
      }}
      showLabels
      className={classes.root}
    >
      <BottomNavigationAction
        classes={{
          root: classes.actionItem,
          selected: classes.selected,
        }}
        label="Belanja"
        icon={<StorefrontIcon className={classes.icon} />}
      />
      <BottomNavigationAction
        classes={{
          root: classes.actionItem,
          selected: classes.selected,
        }}
        label="Transaksi"
        icon={<ReceiptIcon className={classes.icon} />}
      />
      <BottomNavigationAction
        classes={{
          root: classes.actionItem,
          selected: classes.selected,
        }}
        label="Bantuan"
        icon={<LiveHelpIcon className={classes.icon} />}
      />
      <BottomNavigationAction
        classes={{
          root: classes.actionItem,
          selected: classes.selected,
        }}
        label="Profile"
        icon={<PermIdentityIcon className={classes.icon} />}
      />
    </BottomNavigation>
  );
}
