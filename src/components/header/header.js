import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import StoreIcon from "@material-ui/icons/Store";
import SearchIcon from "@material-ui/icons/Search";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import Grid from "@material-ui/core/Grid";
import Fade from "@material-ui/core/Fade";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    position: "fixed",
    zIndex: 999,
    width: "100%",
    maxWidth: 444,

    // "& > *": {
    //   width: 475,
    //   height: 50,
    // },
  },
  paper: {
    borderBottomRightRadius: 5,
    borderBottomLeftRadius: 5,
    width: "100%",
    boxShadow:
      "0px 2px 1px -1px rgba(0,0,0,0.2),0px 1px 1px 0px rgba(0,0,0,0.14),0px 1px 3px 0px rgba(0,0,0,0.12)",
  },
  content: { padding: "6px 20px" },
}));

export default function Header({ onTop }) {
  const classes = useStyles();

  return (
    <Fade in={onTop}>
      <div className={classes.root}>
        <Paper className={classes.paper} variant="outlined">
          <div className={classes.content}>
            <Grid container className={classes.content}>
              <Grid item xs={1}>
                <StoreIcon style={{ color: "rgb(135, 202, 254)", width: 30 }} />
              </Grid>
              <Grid
                item
                xs={6}
                style={{
                  marginLeft: 10,
                  fontWeight: 700,
                  fontFamily: "Montserrat",
                }}
              >
                Pasar Bulu Semarang
              </Grid>
              <Grid item xs={3}>
                <ExpandMoreIcon style={{ color: "rgb(112, 117, 133)" }} />
              </Grid>
              <Grid item xs={1}>
                <SearchIcon style={{ color: "rgb(112, 117, 133)" }} />
              </Grid>
            </Grid>
          </div>
        </Paper>
      </div>
    </Fade>
  );
}
