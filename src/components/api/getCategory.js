export default function GetCategory() {
  return fetch(
    "https://api.tumbasin.id/t/v1/products/categories"
  ).then((data) => data.json());
}
