export default function GetProduct() {
  return fetch(
    "https://api.tumbasin.id/t/v1/products?vendor=1003&featured=true"
  ).then((data) => data.json());
}
