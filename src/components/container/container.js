import React, { useEffect, useState } from "react";
import Container from "@material-ui/core/Container";
import BottomNav from "../bottom-navigation/bottom-navigation";
import { makeStyles } from "@material-ui/core/styles";
import PaperHome from "../paper/paper";
import AppBar from "../appbar/appbar";
import Header from "../header/header";

const useStyles = makeStyles({
  root: {
    display: "flex",
    // justifyContent: "center",
    maxWidth: 444,
    width: "100%",
    justifyContent: "center",
    padding: 0,
    minHeight: "100vh",
    height: "100%",
  },
});

export default function FixedContainer() {
  const [onTop, setOnTop] = useState(true);

  const handleScroll = () => {
    if (window.scrollY < 80) {
      setOnTop(true);
      console.log("Terscroll");
    } else {
      setOnTop(false);
    }
  };
  const classes = useStyles();
  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  });

  return (
    <React.Fragment>
      {/* <CartContext.Provider value={{ value, setValue }}> */}

      <Container maxWidth="xs" className={classes.root}>
        <AppBar onTop={onTop} />
        <Header onTop={!onTop} />
        <PaperHome />
        <BottomNav />
      </Container>

      {/* </CartContext.Provider> */}
    </React.Fragment>
  );
}
