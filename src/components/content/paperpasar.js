import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import { Grid, Typography } from "@material-ui/core";
import ButtonSearch from "../button/buttonsearch";
import StoreIcon from "@material-ui/icons/Store";

const useStyles = makeStyles({
  root: {
    display: "flex",
    justifyContent: "center",
    marginTop: 70,
    position: "absolute",
    zIndex: 999,
    height: 70,
    borderRadius: 7,
    width: "100%",
    // "& > *": {
    //   width: 410,
    //   height: 70,
    //   borderRadius: 7,
    //   padding: "8px 15px",
    // },
  },
  title: {
    fontSize: "0.875rem",
    fontFamily: "Montserrat",
    fontWeight: 400,
    lineHeight: 1.43,
  },
  content: {
    display: "flex",
    marginTop: 10,
    position: "relative",
    bottom: 0,
  },
  icon: {
    color: "rgb(135, 202, 254)",
    fontSize: 30,
  },
});

export default function PaperPasar() {
  const classes = useStyles();
  return (
    <>
      <div className={classes.root}>
        <div style={{ width: "90%" }}>
          <Paper variant="outlined" style={{ padding: 10 }}>
            <Typography className={classes.title}>Kamu Belanja Di:</Typography>

            <Grid container>
              <Grid item xs={2}>
                <StoreIcon className={classes.icon} />
              </Grid>
              <Grid item xs={7}>
                <Typography
                  style={{
                    marginTop: 5,
                    fontSize: 14,
                    fontWeight: 500,
                  }}
                >
                  Pasar Bulu Semarang
                </Typography>
              </Grid>
              <Grid item xs={3}>
                <ButtonSearch />
              </Grid>
            </Grid>
          </Paper>
        </div>
      </div>
    </>
  );
}
