import React from "react";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import MobileStepper from "@material-ui/core/MobileStepper";
import SwipeableViews from "react-swipeable-views";
import { autoPlay } from "react-swipeable-views-utils";
import { useQuery } from "react-query";
import SkeletonBanner from "../skeleton/skeletonBanner";
import CircularProgress from "@material-ui/core/CircularProgress";

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginTop: 40,
    padding: "0 10px",
  },
  img: {
    height: 175,
    overflow: "hidden",
    borderRadius: 8,
  },

  active: {
    borderRadius: 10,
    backgroundColor: "white",
  },
}));

const getBanner = async () => {
  const URL =
    "https://dashboard.tumbasin.id/wp-json/wp/v2/media?media_category[]=39";
  const response = await fetch(URL);
  if (!response.ok) {
    throw new Error("Gagal broo");
  }
  return await response.json();
};

function PaperSlider(props) {
  const classes = useStyles();
  const theme = useTheme();
  const [activeStep, setActiveStep] = React.useState(0);
  // const [list, setList] = useState([]);
  const handleStepChange = (step) => {
    setActiveStep(step);
  };

  const { data, isLoading, isError, isSuccess, isFetching } = useQuery(
    "banner",
    getBanner,
    {
      staleTime: 3000,
      refetchInterval: 6000,
    }
  );
  // useEffect(() => {
  //   axios
  //     .get(
  //       "https://dashboard.tumbasin.id/wp-json/wp/v2/media?media_category[]=39"
  //     )
  //     .then((response) => setList(response.data));
  // }, []);

  return (
    <div>
      <div className={classes.root}>
        {isFetching && (
          <CircularProgress style={{ position: "fixed", top: 10, right: 10 }} />
        )}
        {isLoading && <SkeletonBanner />}
        {isError && <p>Terdapat kesalahan dalam memproses permintaan anda</p>}
        <AutoPlaySwipeableViews
          axis={theme.direction === "rtl" ? "x-reverse" : "x"}
          index={activeStep}
          onChangeIndex={handleStepChange}
          enableMouseEvents
        >
          {isSuccess &&
            data?.map((banner, index) => (
              <div key={banner.label}>
                {Math.abs(activeStep - index) <= 2 ? (
                  <img
                    className={classes.img}
                    src={banner.guid.rendered}
                    alt={banner.label}
                  />
                ) : null}
              </div>
            ))}
        </AutoPlaySwipeableViews>
      </div>
      <MobileStepper
        steps={3}
        position="static"
        variant="dots"
        activeStep={activeStep}
        classes={{ dotActive: classes.active }}
        style={{
          position: "relative",
          bottom: 20,
          background: "transparent",
          cursor: "pointer",
        }}
      />
    </div>
  );
}

export default PaperSlider;
