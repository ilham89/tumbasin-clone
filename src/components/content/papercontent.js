import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import { Typography } from "@material-ui/core";

const useStyles = makeStyles({
  root: {
    display: "flex",
    padding: "0px 15px",
    "& > *": {
      width: 50,
      height: 50,
    },
  },
  content: {
    marginTop: 10,
    borderRadius: 5,
    border: "none",
    boxShadow: "0px 1px 2px #dbdbdb",
    backgroundColor: "#FDF4FC",
  },
  contentdown: {
    marginTop: 30,
    borderRadius: 5,
    border: "none",
    boxShadow: "0px 1px 2px #dbdbdb",
    backgroundColor: "#f7e9f6",
  },
  desc: {
    fontSize: 9,
    marginTop: 55,
    textAlign: "center",
    color: "rgb(112, 117, 133)",
  },
});

export default function PaperContent() {
  const classes = useStyles();

  return (
    <>
      <div className={classes.root}>
        <Paper variant="outlined" className={classes.content}>
          <Typography className={classes.desc}>Sayuran</Typography>
        </Paper>
        <Paper variant="outlined" className={classes.content}>
          <Typography className={classes.desc}>Buah</Typography>
        </Paper>
        <Paper variant="outlined" className={classes.content}>
          <Typography className={classes.desc}>Lauk Pauk</Typography>
        </Paper>
        <Paper variant="outlined" className={classes.content}>
          <Typography className={classes.desc}>Seafood</Typography>
        </Paper>
        <Paper variant="outlined" className={classes.content}>
          <Typography className={classes.desc}>Bumbu</Typography>
        </Paper>
      </div>
      <div className={classes.root}>
        <Paper variant="outlined" className={classes.contentdown}>
          <Typography className={classes.desc}>Sembako</Typography>
        </Paper>
        <Paper variant="outlined" className={classes.contentdown}>
          <Typography className={classes.desc}>Jajanan</Typography>
        </Paper>
        <Paper variant="outlined" className={classes.contentdown}>
          <Typography className={classes.desc}>Kesehatan</Typography>
        </Paper>
        <Paper variant="outlined" className={classes.contentdown}>
          <Typography className={classes.desc}>Siap Masak</Typography>
        </Paper>
      </div>
    </>
  );
}
