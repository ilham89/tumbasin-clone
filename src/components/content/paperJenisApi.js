import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import { Typography } from "@material-ui/core";

const useStyles = (theme) => ({
  root: {
    flexGrow: 1,
    marginLeft: 10,
    marginTop: 10,
  },
  paper: {
    textAlign: "center",
    color: "rgb(112, 117, 133)",
    width: 45,
    height: 45,
  },
  img: {
    width: 40,
    height: 40,
  },
  text: {
    fontSize: 9,
    fontFamily: "Montserrat",
    fontWeight: 400,
    lineHeight: 1.66,
    marginTop: 5,
  },
});

class PaperJenisApi extends Component {
  constructor(props) {
    super(props);

    this.state = {
      category: [],
      response: "",
      display: "none",
    };
  }
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Grid container>
          <Grid item xs={2.4}>
            <Paper className={classes.paper}>
              <img
                className={classes.img}
                src="https://dashboard.tumbasin.id/wp-content/uploads/2019/10/sayuran.png"
                alt="Sayuran"
              />
              <Typography className={classes.text}> Sayuran </Typography>
            </Paper>
          </Grid>
          <Grid item xs={2.4}>
            <Paper className={classes.paper}>
              <img
                className={classes.img}
                src="https://dashboard.tumbasin.id/wp-content/uploads/2019/10/Group-4-1.png"
                alt="Buah"
              />
              <Typography className={classes.text}>Buah</Typography>
            </Paper>
          </Grid>
          <Grid item xs={2.4}>
            <Paper className={classes.paper}>
              <img
                className={classes.img}
                src="https://dashboard.tumbasin.id/wp-content/uploads/2019/10/lauk_pauk.png"
                alt="Lauk Pauk"
              />
              <Typography className={classes.text}> Lauk Pauk</Typography>
            </Paper>
          </Grid>
          <Grid item xs={2.4}>
            <Paper className={classes.paper}>
              <img
                className={classes.img}
                src="https://dashboard.tumbasin.id/wp-content/uploads/2019/10/seafood.png"
                alt="Seafood"
              />
              <Typography className={classes.text}> Seafood</Typography>
            </Paper>
          </Grid>
          <Grid item xs={2.4}>
            <Paper className={classes.paper}>
              <img
                className={classes.img}
                src="https://dashboard.tumbasin.id/wp-content/uploads/2019/10/bumbu.png"
                alt="Bumbu"
              />
              <Typography className={classes.text}>Bumbu</Typography>
            </Paper>
          </Grid>

          <Grid item xs={2.4}>
            <Paper className={classes.paper}>
              <img
                className={classes.img}
                src="https://dashboard.tumbasin.id/wp-content/uploads/2019/10/sembako.png"
                alt="Sembako"
              />
              <Typography className={classes.text}>Sembako</Typography>
            </Paper>
          </Grid>
          <Grid item xs={2.4}>
            <Paper className={classes.paper}>
              <img
                className={classes.img}
                src="https://dashboard.tumbasin.id/wp-content/uploads/2019/10/jajanan.png"
                alt="Jajanan"
              />
              <Typography className={classes.text}>Jajanan</Typography>
            </Paper>
          </Grid>
          <Grid item xs={2.4}>
            <Paper className={classes.paper}>
              <img
                className={classes.img}
                src="https://dashboard.tumbasin.id/wp-content/uploads/2020/06/healthy_64.png"
                alt="Kesehatan"
              />
              <Typography className={classes.text}>Kesehatan</Typography>
            </Paper>
          </Grid>
          <Grid item xs={2.4}>
            <Paper className={classes.paper}>
              <img
                className={classes.img}
                src="https://dashboard.tumbasin.id/wp-content/uploads/2020/08/wok-2.png"
                alt="Siap Masak"
              />
              <Typography className={classes.text}>Siap Masak</Typography>
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default withStyles(useStyles, { withTheme: true })(PaperJenisApi);
