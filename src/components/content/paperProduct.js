import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import ButtonAdd from "../button/buttonAdd";
import Typography from "@material-ui/core/Typography";
import CurrentFormatter from "../currency-formatter";
import Divider from "@material-ui/core/Divider";
import { useQuery } from "react-query";
import SkeletonProduct from "../skeleton/skeletonProduct";
import CircularProgress from "@material-ui/core/CircularProgress";

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: 15,
    marginBottom: 15,
  },
  img: {
    width: 105,
    height: 80,
    backgroundPosition: "center",
    backgroundSize: "cover",
  },
}));

const getProduct = async () => {
  const URL = "https://api.tumbasin.id/t/v1/products?vendor=1003&featured=true";
  const response = await fetch(URL);
  if (!response.ok) {
    throw new Error("Gagal broo");
  }
  return await response.json();
};

export default function PaperProduct() {
  const { data, isLoading, isError, isSuccess, isFetching } = useQuery(
    "product",
    getProduct,
    {
      staleTime: 3000,
      refetchInterval: 6000,
    }
  );
  const classes = useStyles();

  return (
    <div className={classes.root}>
      {isFetching && (
        <CircularProgress style={{ position: "fixed", top: 10, right: 10 }} />
      )}
      {isLoading && <SkeletonProduct />}
      {isError && <p>Terdapat kesalahan dalam memproses permintaan anda</p>}
      {isSuccess &&
        data?.slice(0, 5).map((product) => (
          <div>
            <Grid container style={{ marginTop: 10, padding: "0 10px" }}>
              <Grid item xs={4}>
                <img
                  className={classes.img}
                  alt={product.slug}
                  src={product.images[0].src}
                />
              </Grid>
              <Grid item xs={5}>
                <Typography>
                  <p style={{ fontSize: 12, marginBottom: 20 }}>
                    {product.name}
                  </p>
                  <p
                    style={{
                      fontSize: 12,
                      color: "#F1885B",
                      fontWeight: "bold",
                    }}
                  >
                    {CurrentFormatter.format(product.price)}
                    <b
                      style={{
                        fontSize: 10,
                        fontWeight: 700,
                        color: "rgb(199, 199, 201)",
                        marginLeft: 5,
                      }}
                    >
                      /{product.meta_data[0].value}
                    </b>
                  </p>
                </Typography>
              </Grid>
              <Grid item xs={3} style={{ marginTop: 50 }}>
                <ButtonAdd data={product} />
              </Grid>
            </Grid>
            <Divider
              style={{
                marginTop: 15,
              }}
            />
          </div>
        ))}
    </div>
  );
}
