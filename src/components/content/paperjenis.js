import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import { Typography } from "@material-ui/core";
import CircularProgress from "@material-ui/core/CircularProgress";
import SkeletonCategory from "../skeleton/skeletonCategory";
import { useQuery } from "react-query";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginLeft: 10,
    marginTop: 10,
  },
  paper: {
    textAlign: "center",
    color: "rgb(112, 117, 133)",
    width: 45,
    height: 45,
  },
  img: {
    width: 40,
    height: 40,
  },
  text: {
    fontSize: 9,
    fontFamily: "Montserrat",
    fontWeight: 400,
    lineHeight: 1.66,
    marginTop: 5,
  },
}));

const getCategory = async () => {
  const URL = "https://api.tumbasin.id/t/v1/products/categories";
  const response = await fetch(URL);
  if (!response.ok) {
    throw new Error("Gagal broo");
  }
  return await response.json();
};

export default function PaperJenis(props) {
  const classes = useStyles();
  // const [loading, setLoading] = useState(true);
  const { data, isLoading, isError, isSuccess, isFetching } = useQuery(
    "category",
    getCategory,
    {
      staleTime: 3000,
      refetchInterval: 6000,
    }
  );

  return (
    <div className={classes.root}>
      {/* {loading ? (
        <SkeletonCategory />
      ) : ( */}
      {isLoading && <SkeletonCategory />}
      {isError && <p>Terdapat kesalahan dalam memproses permintaan anda</p>}
      {isFetching && (
        <CircularProgress style={{ position: "fixed", top: 10, right: 10 }} />
      )}
      <div>
        <Typography
          style={{ marginBottom: 10, padding: "0 6px", fontWeight: "bold" }}
        >
          Telusuri jenis produk
        </Typography>
        <Grid container wrap="wrap" style={{ padding: "0 5px" }}>
          {isSuccess &&
            data?.slice(0, 5).map((category) => (
              <Grid item xs={2} style={{ marginLeft: 8 }}>
                <Paper className={classes.paper}>
                  <img
                    className={classes.img}
                    src={category.image.src}
                    alt={category.slug}
                  />
                  <Typography className={classes.text}>
                    {category.name}
                  </Typography>
                </Paper>
              </Grid>
            ))}
        </Grid>
        <Grid
          container
          wrap="wrap"
          style={{ margin: "36px 0", padding: "0 5px" }}
        >
          {data?.slice(5, 11).map((category) => (
            <Grid item xs={2} style={{ marginLeft: 8 }}>
              <Paper className={classes.paper}>
                <img
                  className={classes.img}
                  src={category.image.src}
                  alt={category.slug}
                />
                <Typography className={classes.text}>
                  {category.name}
                </Typography>
              </Paper>
            </Grid>
          ))}
        </Grid>
      </div>
    </div>
  );
}
