import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: "0 10px",
  },
}));

export default function TitleProduct() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid container justify="space-between">
        <Grid item xs={8} style={{ fontWeight: "bold" }}>
          Produk Terlaris
        </Grid>
        <Grid item xs={3.8} style={{ fontWeight: "bold", color: "#F15B5D" }}>
          Lihat Semua
        </Grid>
      </Grid>
    </div>
  );
}
