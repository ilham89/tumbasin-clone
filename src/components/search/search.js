import React from "react";
import Toolbar from "@material-ui/core/Toolbar";
import InputBase from "@material-ui/core/InputBase";
import { makeStyles } from "@material-ui/core/styles";
import SearchIcon from "@material-ui/icons/Search";
import InputAdornment from "@material-ui/core/InputAdornment";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    display: "flex",
    justifyContent: "center",
    width: "100%",
  },
  inputRoot: {
    color: "inherit",
    border: "1px solid white",
    borderRadius: 20,
    width: "100%",
    padding: "0 12px",
  },
  inputInput: {
    width: "100%",
  },
  search: {
    width: "100%",
  },
}));

export default function SearchAppBar() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Toolbar style={{ width: "100%" }}>
        <div className={classes.search}>
          <InputBase
            placeholder="Search…"
            classes={{
              root: classes.inputRoot,
              input: classes.inputInput,
            }}
            style={{ background: "#fff" }}
            startAdornment={
              <InputAdornment>
                <SearchIcon style={{ paddingRight: "10px" }} />
              </InputAdornment>
            }
          />
        </div>
      </Toolbar>
    </div>
  );
}
