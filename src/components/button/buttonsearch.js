import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles((theme) => ({
  root: {
    "& > *": {
      height: 30,
    },
  },
}));

export default function ButtonSearch() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Button
        variant="contained"
        style={{
          backgroundColor: "rgb(241, 91, 93)",
          color: "white",
          width: 60,
        }}
      >
        Ganti
      </Button>
    </div>
  );
}
