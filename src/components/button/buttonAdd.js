import React, { useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import { Typography } from "@material-ui/core";
import { CartContext } from "../../context/cartCantext";

const useStyles = makeStyles((theme) => ({
  root: {},
  text: {
    fontSize: 10,
  },
}));

export default function ButtonAdd(props) {
  const classes = useStyles();

  const { addCart, cart, increaseCart, decreaseCart } = useContext(CartContext);
  const filterCart = cart.filter((item) => item.id === props.data.id);

  // const addToCart = () => {
  //   const product = { name: props.name, price: props.price };
  //   setValue((curr) => [curr, product]);
  //   console.log("terclick");
  // };
  // function tambah() {
  //   setQuantity(quantity + 1);
  //   setValue(value + 1);
  //   // addToCart();
  // }
  // function kurang() {
  //   if (quantity > 0) {
  //     setQuantity(quantity - 1);
  //     setValue(value - 1);
  //   }
  // }

  return (
    <div className={classes.root}>
      {filterCart[0]?.total > 0 ? (
        <div>
          <div style={{ display: "flex" }}>
            <Button
              variant="outlined"
              size="small"
              style={{
                minWidth: 29,
                height: 25,
                borderRadius: 4,
                border: "1px solid rgba(0, 0, 0, 0.23)",
              }}
              onClick={() => decreaseCart(props.data)}
            >
              <Typography> - </Typography>
            </Button>
            <Typography style={{ padding: "0px 5px" }}>
              {filterCart[0]?.total}
            </Typography>
            <Button
              variant="contained"
              size="small"
              style={{
                minWidth: 29,
                height: 25,
                color: "white",
                backgroundColor: "rgb(241, 91, 93)",
                borderRadius: 4,
                border: "1px solid rgba(0, 0, 0, 0.23)",
              }}
              // onClick={tambah}
              onClick={() => increaseCart(props.data)}
            >
              <Typography> + </Typography>
            </Button>
          </div>
          {/* <Cart /> */}
        </div>
      ) : (
        <Button
          onClick={() => addCart(props.data)}
          variant="contained"
          style={{
            backgroundColor: "rgb(241, 91, 93)",
            color: "white",
            width: 80,
            height: 30,
          }}
        >
          <p className={classes.text}>Tambahkan</p>
        </Button>
      )}
    </div>
  );
}
