import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import PaperSlider from "../content/paperslider";
import PaperJenis from "../content/paperjenis";
import TitleProduct from "../content/titleProduct";
import PaperProduct from "../content/paperProduct";
import Cart from "../cart/cart";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    marginTop: 60,
    position: "static",
    width: "100%",
    height: 1190,
    maxWidth: 444,
    // "& > *": {
    //   width: 445,
    //   height: 1300,
    //   padding: "60px 15px",
    // },
  },
  font: {
    fontWeight: 300,
    marginLeft: 5,
  },
  container: {
    marginTop: 110,
    width: "100%",
    maxWidth: 444,
  },
  product: {},
}));

export default function PaperHome(props) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Paper variant="outlined" style={{ width: "100%", maxWidth: 444 }}>
        <div className={classes.container}>
          <PaperJenis />
          <PaperSlider />
          <TitleProduct />
          <Cart />
          <PaperProduct />
        </div>
      </Paper>
    </div>
  );
}
